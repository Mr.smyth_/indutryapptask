package com.example.IndustryApps.mapper;
import com.example.IndustryApps.dto.outputs.Assets;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Mapper
public interface AssetResponseMapper {
    AssetResponseMapper INSTANCE = Mappers.getMapper(AssetResponseMapper.class);

    default List<Assets> toAssetList(ArrayList<Map<String, Object>> response){
        List<Map<String, Object>> assets = response.stream()
                .map(keyValuePair -> (Map<String, Object>) keyValuePair.get("asset"))
                .collect(Collectors.toList());
        List<Assets> assetsList = new ArrayList<>();
        for (Map<String, Object> entry:assets){
            List<Map<String, Object>> identification = new ArrayList<>();
            List<Map<String, Object>> assetExtra = new ArrayList<>();
            identification.add((Map<String, Object>) entry.get("identification"));
            assetExtra.add((Map<String, Object>) entry.get("assetExtras"));
            Assets assetOut = new Assets();
            for (Map<String, Object> id:identification ){
                assetOut.setAssetCode((String) id.get("idType"));
            }
            for (Map<String, Object> id:assetExtra ){
                assetOut.setAssetName((String) id.get("assetName"));
            }
            assetsList.add(assetOut);
        }
        return assetsList;
    }

}
