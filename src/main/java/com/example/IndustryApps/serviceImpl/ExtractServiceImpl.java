package com.example.IndustryApps.serviceImpl;

import com.example.IndustryApps.dto.internal.AuthToken;
import com.example.IndustryApps.dto.outputs.Assets;
import com.example.IndustryApps.mapper.AssetResponseMapper;
import com.example.IndustryApps.service.ExtractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExtractServiceImpl implements ExtractService {

    @Autowired
    RestTemplate restTemplate;

    @Override
    public List<Assets> getExtractedData() {
        String accessToken = getToken();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer " + accessToken);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ArrayList response = restTemplate.exchange("https://aasgateway.uat.industryapps.net/aasList?PlantCode=SB01&amp;AssetType=machine",
                HttpMethod.GET, entity, ArrayList.class).getBody();
        return AssetResponseMapper.INSTANCE.toAssetList(response);
    }

    private String getToken(){
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", "client_credentials");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.set("Content-Type", "application/x-www-form-urlencoded");
        httpHeaders.set("Authorization", "Basic:ZGlnaXRhbC5jb250cm9sLmNlbnRyZTo3YzVjYmQ3Zi1kMWM1LTQwZTQtYTE3NS0yNWQ2MDI4YjIwZWQ=");
        HttpEntity<?> entity = new HttpEntity<>(requestBody, httpHeaders);
        AuthToken authToken = restTemplate.exchange("https://auth.uat.industryapps.net/auth/realms/IndustryApps/protocol/openid-connect/token",
                HttpMethod.POST, entity, AuthToken.class).getBody();
        return authToken.getAccess_token();
    }

}
