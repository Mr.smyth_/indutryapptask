package com.example.IndustryApps.service;

import com.example.IndustryApps.dto.outputs.Assets;
import org.json.JSONObject;

import java.util.List;

public interface ExtractService {


    List<Assets> getExtractedData();
}
