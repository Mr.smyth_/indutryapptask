package com.example.IndustryApps.Controller;

import com.example.IndustryApps.constants.UrlConstants;
import com.example.IndustryApps.dto.outputs.Assets;
import com.example.IndustryApps.service.ExtractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExtractController {
    @Autowired
    ExtractService extractService;

    @GetMapping(value = UrlConstants.GET_EXTRACTED_DATA)
    private List<Assets> getExtractedData(){
        return extractService.getExtractedData();
    }

}
